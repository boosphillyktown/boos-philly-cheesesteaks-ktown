Colorful sandwich joint with a menu of traditional Philadelphia-style comfort classics. Call (213) 388-1955 for more information!

Address: 3377 Wilshire Blvd, #103, Los Angeles, CA 90010, USA

Phone: 213-388-1955

Website: https://boosphilly.com